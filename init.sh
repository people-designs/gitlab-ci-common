#!/bin/sh
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
echo

google_account=`gcloud config list account --format "value(core.account)"`

if [ -z `gcloud config list account --format "value(core.account)"` ]; then
  echo "${red}You do not currently have an active Google account selected. Please run:

  $ gcloud auth login${reset}"
  exit 1
fi

read -p "${blue}Enter the Google project name [${PWD##*/}]: ${reset}" project_name
project_name=${project_name:-${PWD##*/}}

gcloud projects describe $project_name >/dev/null 2>&1
google_project_check=$?

if [[ $google_project_check != 0 ]]; then
  read -s -p "${yellow}Google project does not exist. Create it? (y/n)? ${reset}" choice
  case "$choice" in
    [Yy]|[[yY][eE][sS] ) echo "yes";;
    * ) echo "${red}No Google project${reset}"; return;;
  esac
fi
